package secretmessage

import (
	"sort"
	"strings"
)

type CharCount struct {
	char  string
	count int
}

// Decode func
func Decode(encoded string) string {
	var letterCount = map[string]int{}
	for _, char := range strings.Split(encoded, "") {
		letterCount[char]++
	}
	var charCounts []CharCount
	for key, val := range letterCount {
		charCounts = append(charCounts, CharCount{key, val})
	}
	sort.Slice(charCounts, func(i, j int) bool {
		return charCounts[i].count > charCounts[j].count
	})
	result := ""
	for _, val := range charCounts {
		result += val.char
	}
	ind := strings.Index(result, "_")
	return result[:ind]
}
