package sumdecimal

import (
	"math/big"
	"strings"
)

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func SumDecimal(c int) int {
	if c < 0 {
		return 0
	}
	number := big.NewFloat(float64(c))
	result := big.NewFloat(0).SetPrec(3500).Sqrt(number)
	var resultStr = result.Text('f', 1100)
	startIndex := strings.Index(resultStr, ".")
	sum := 0
	for i := startIndex + 1; i < min(len(resultStr), startIndex+1001); i++ {
		sum += int(resultStr[i]) - '0'
	}
	return sum
}
