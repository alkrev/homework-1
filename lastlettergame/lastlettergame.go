package lastlettergame

import "strings"

func SearchWordsR(index int, maxNumber *int, dic []string, used map[string]bool, current []string, result []string) {
	for _, val := range dic {
		val = strings.Trim(val, "\r")
		if inUse := used[val]; !inUse {
			if index == 0 || current[index-1][len(current[index-1])-1:][0] == val[0:1][0] {
				used[val] = true
				current[index] = val
				if index+1 > *maxNumber {
					*maxNumber = index + 1
					for i := 0; i < *maxNumber; i++ {
						result[i] = current[i]
					}
				}
				if *maxNumber == len(dic) {
					break
				}
				SearchWordsR(index+1, maxNumber, dic, used, current, result)
				current[index] = ""
				used[val] = false
			}
		}
	}
}

func Sequence(dic []string) []string {
	var result = make([]string, len(dic))
	var current = make([]string, len(dic))
	var maxNumber = 0
	var used map[string]bool = map[string]bool{}
	SearchWordsR(0, &maxNumber, dic, used, current, result)
	return result[:maxNumber]
}
