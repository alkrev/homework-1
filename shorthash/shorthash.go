package shorthash

func GetHashes(hash string, max int, dictionary []rune, hashes *[]string) {
	length := len([]rune(hash))
	if length == max {
		*hashes = append(*hashes, hash)
		return
	} else {
		for i := 0; i < len(dictionary); i++ {
			GetHashes(hash+string(dictionary[i]), max, dictionary, hashes)
		}
	}
}

func GenerateShortHashes(dictionary string, len int) []string {
	result := []string{}
	dict := []rune(dictionary)
	for i := 1; i <= len; i++ {
		var hashes []string
		GetHashes("", i, dict, &hashes)
		result = append(result, hashes...)
	}
	return result
}
