package floyd

// Triangle makes a Floyd's triangle matrix with rows count.
func Triangle(rows int) [][]int {
	result := [][]int{}
	counter := 1
	for i := 0; i < rows; i++ {
		result = append(result, make([]int, i+1))
		for j := 0; j <= i; j++ {
			result[i][j] = counter
			counter++
		}
	}
	return result
}
