package nodedegree

import (
	"fmt"
)

// Degree func
func Degree(nodes int, graph [][2]int, node int) (int, error) {
	if node > nodes || node < 1 {
		return 0, fmt.Errorf("node %d not found in the graph", node)
	}
	svMatrix := make([][]bool, nodes)
	for i := 0; i < nodes; i++ {
		svMatrix[i] = make([]bool, nodes)
	}
	for _, val := range graph {
		svMatrix[val[0]-1][val[1]-1] = true
		svMatrix[val[1]-1][val[0]-1] = true
	}
	result := 0
	for j := 0; j < nodes; j++ {
		if node-1 != j && svMatrix[node-1][j] {
			result++
		}
	}
	return result, nil
}
