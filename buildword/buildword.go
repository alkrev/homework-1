package buildword

import "strings"

func getLowestNumberR(word string, fragments []string, current int, number *int) {
	if len(word) > 0 {
		for i := 0; i < len(fragments); i++ {
			if ind := strings.Index(word, fragments[i]); ind == 0 {
				getLowestNumberR(word[len(fragments[i]):], fragments, current+1, number)
			}
		}
	} else {
		if *number == 0 || *number > current {
			*number = current
		}
	}
}

func BuildWord(word string, fragments []string) int {
	var number int
	getLowestNumberR(word, fragments, 0, &number)
	return number
}
