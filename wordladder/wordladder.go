package wordladder

func CompareStr(str1, str2 string) bool {
	count := 0
	for i := 0; i < len(str1); i++ {
		if str1[i] != str2[i] {
			count++
		}
	}
	return count == 1
}

func FindShortestR(to string, dic []string, used map[string]bool, str string, current int, result *int) {
	if str == to {
		if current < *result {
			*result = current
		}
		return
	} else {

		for i := 0; i < len(dic); i++ {
			if _, ok := used[dic[i]]; !ok && CompareStr(str, dic[i]) {
				used[dic[i]] = true
				FindShortestR(to, dic, used, dic[i], current+1, result)
				delete(used, dic[i])
			}
		}
	}
}

func WordLadder(from string, to string, dic []string) int {
	result := len(dic) + 1
	used := map[string]bool{}
	FindShortestR(to, dic, used, from, 1, &result)
	if result == len(dic)+1 {
		return 0
	} else {
		return result
	}
}
