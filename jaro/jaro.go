package jaro

import (
	"sort"
	"strings"
)

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func Distance(word1 string, word2 string) float64 {
	word1 = strings.ToLower(word1)
	word2 = strings.ToLower(word2)
	if word1 == word2 {
		return 1.
	}
	ds := max(max(len(word1), len(word2))/2-1, 0) // Максимальное расстояние между совпадающими символами
	m := 0                                        // Число совпадающих символов
	word1Map := map[int]byte{}
	word2Map := map[int]byte{}

	for i := 0; i < len(word1); i++ {
		for j := max(0, i-ds); j < min(len(word2), i+ds+1); j++ {
			if word1[i] == word2[j] {
				if _, ok := word2Map[j]; !ok {
					m++
					word1Map[i] = word1[i]
					word2Map[j] = word2[j]
					break
				}
			}
		}
	}

	if m == 0 {
		return 0
	}

	var word1Mapkeys []int
	for key := range word1Map {
		word1Mapkeys = append(word1Mapkeys, key)
	}
	sort.Ints(word1Mapkeys)
	word1Match := ""
	for _, val := range word1Mapkeys {
		word1Match += string(word1[val])
	}
	var word2Mapkeys []int
	for key := range word2Map {
		word2Mapkeys = append(word2Mapkeys, key)
	}
	sort.Ints(word2Mapkeys)
	word2Match := ""
	for _, val := range word2Mapkeys {
		word2Match += string(word2[val])
	}

	t := 0 // Число транспозиций
	for i := 0; i < min(len(word1Match), len(word2Match)); i++ {
		if word1Match[i] != word2Match[i] {
			t++
		}
	}
	t = t / 2

	mf := float64(m)
	d := (1. / 3.) * (mf/float64(len(word1)) + mf/float64(len(word2)) + (mf-float64(t))/mf) // Сходство Джаро
	return d
}
