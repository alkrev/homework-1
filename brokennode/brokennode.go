package brokennode

import "math/bits"

func checkVariant(variant uint, reports []bool) ([]bool, bool) {
	var nodes = make([]bool, len(reports))
	for i := 0; i < len(reports); i++ {
		nodes[i] = variant&uint(1) == 1
		variant = variant >> 1
	}
	for i := 0; i < len(reports); i++ {
		if !nodes[i] {
			// не broken
			if reports[i] {
				// следующий должен быть не broken
				if nodes[(i+1)%len(reports)] {
					return nil, false // broken - вариант не подходит
				}
			} else {
				// следующий должен быть broken
				if !nodes[(i+1)%len(reports)] {
					return nil, false // не broken - вариант не подходит
				}
			}
		}
	}
	return nodes, true
}

func searchVariants(brokenNodes int, reports []bool) [][]bool {
	var result [][]bool
	var max = uint(1) << len(reports)
	var i uint = 0
	for ; i < max; i++ {
		if bits.OnesCount(i) == brokenNodes {
			if variant, ok := checkVariant(i, reports); ok {
				result = append(result, variant)
			}
		}
	}
	return result
}

func defineResult(variants [][]bool, length int) string {
	result := ""
	for i := 0; i < length; i++ {
		count := 0
		for j := 0; j < len(variants); j++ {
			if variants[j][i] {
				count++
			}
		}
		switch {
		case count == len(variants):
			{
				result += "B"
			}
		case count == 0:
			{
				result += "W"
			}
		default:
			{
				result += "?"
			}
		}
	}
	return result
}

func FindBrokenNodes(brokenNodes int, reports []bool) string {
	var variants [][]bool = searchVariants(brokenNodes, reports)
	return defineResult(variants, len(reports))
}
