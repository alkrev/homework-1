package mergesort

// MergeSort is used to sort an array of integer
func MergeSort(input []int) []int {
	if len(input) > 1 {
		first := MergeSort(input[:len(input)/2])
		second := MergeSort(input[len(input)/2:])
		var result []int
		f := 0
		s := 0
		for f < len(first) || s < len(second) {
			switch {
			case f == len(first):
				{
					result = append(result, second[s:]...)
					s = len(second)
				}
			case s == len(second):
				{
					result = append(result, first[f:]...)
					f = len(first)
				}
			default:
				{
					if second[s : s+1][0] < first[f : f+1][0] {
						result = append(result, second[s : s+1][0])
						s++
					} else {
						result = append(result, first[f : f+1][0])
						f++
					}
				}
			}
		}
		return result
	} else {
		return input
	}
}
