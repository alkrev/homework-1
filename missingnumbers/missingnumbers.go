package missingnumbers

func Missing(numbers []int) []int {
	var result []int
	var tmp = map[int]bool{}
	for _, val := range numbers {
		tmp[val] = true
	}
	for i := 1; i <= len(numbers)+2; i++ {
		if _, ok := tmp[i]; !ok {
			result = append(result, i)
			if len(result) > 1 {
				break
			}
		}
	}
	return result
}
