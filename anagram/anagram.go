package anagram

import (
	"sort"
	"strings"
)

func lowerCaseSortAndTrim(word string) string {
	word = strings.ToLower(word)
	letters := strings.Split(word, "")
	sort.Strings(letters)
	result := strings.Join(letters, "")
	result = strings.Trim(result, " ")
	return result
}

func FindAnagrams(dictionary []string, word string) (result []string) {
	wordTrimmed := strings.Trim(word, " ")
	wordTrimmedLower := strings.ToLower(wordTrimmed)
	testWord := lowerCaseSortAndTrim(word)
	if len(testWord) > 0 {
		for _, dictWord := range dictionary {
			dictWordTrimmed := strings.Trim(dictWord, " \r")
			dictWordTrimmedLower := strings.ToLower(dictWordTrimmed)
			if wordTrimmedLower != dictWordTrimmedLower {
				testDictWord := lowerCaseSortAndTrim(dictWordTrimmedLower)
				if testWord == testDictWord {
					result = append(result, dictWordTrimmed)
				}
			}
		}
	}
	return result
}
