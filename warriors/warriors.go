package warriors

func CopyMatr(source [][]byte, dest [][]byte, size int) {
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			dest[i][j] = source[i][j]
		}
	}
}

func MultiplyMatrix(dostMatr, svMatr, resultMatr [][]byte, size int) {
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			resultMatr[i][j] = 0
			for k := 0; k < size; k++ {
				resultMatr[i][j] += dostMatr[k][j] * svMatr[i][k]
			}
			if resultMatr[i][j] > 0 {
				resultMatr[i][j] = 1
			}
		}
	}
}

func Count(image string) int {
	if len(image) > 0 {
		var bitImage [][]byte
		line := 0
		for i := 0; i < len(image); i++ {
			if len(bitImage) <= line {
				bitImage = append(bitImage, []byte{})
			}
			if image[i] == '\n' {
				line++
			} else {
				bitImage[line] = append(bitImage[line], image[i]-'0')
			}
		}
		line = line + 1
		size := (line) * (line)
		svMatr := make([][]byte, size)
		dostMatr := make([][]byte, size)
		resultMatr := make([][]byte, size)
		for i := 0; i < size; i++ {
			svMatr[i] = make([]byte, size)
			dostMatr[i] = make([]byte, size)
			resultMatr[i] = make([]byte, size)
		}
		for i := 0; i < line; i++ {
			for j := 0; j < line; j++ {
				if bitImage[i][j] == 1 {
					svMatr[i*line+j][i*line+j] = 1
					if i+1 < line && bitImage[i+1][j] == 1 {
						svMatr[i*line+j][(i+1)*line+j] = 1
						svMatr[(i+1)*line+j][i*line+j] = 1
					}
					if i+1 < line && j+1 < line && bitImage[i+1][j+1] == 1 {
						svMatr[i*line+j][(i+1)*line+j+1] = 1
						svMatr[(i+1)*line+j+1][i*line+j] = 1
					}
					if j+1 < line && bitImage[i][j+1] == 1 {
						svMatr[i*line+j][i*line+j+1] = 1
						svMatr[i*line+j+1][i*line+j] = 1
					}
					if i+1 < line && j-1 >= 0 && bitImage[i+1][j-1] == 1 {
						svMatr[i*line+j][(i+1)*line+j-1] = 1
						svMatr[(i+1)*line+j-1][i*line+j] = 1
					}
				}
			}
		}
		CopyMatr(svMatr, dostMatr, size)
		for k := 1; k < size; k++ {
			MultiplyMatrix(dostMatr, svMatr, resultMatr, size)
			CopyMatr(resultMatr, dostMatr, size)
		}
		result := 0
		ignoreLines := map[int]bool{}
		for i := 0; i < size; i++ {
			if !ignoreLines[i] && dostMatr[i][i] > 0 {
				for j := i; j < size; j++ {
					if i != j && dostMatr[i][j] > 0 {
						ignoreLines[j] = true
					}
				}
				result++
			}
		}
		return result
	} else {
		return 0
	}
}
