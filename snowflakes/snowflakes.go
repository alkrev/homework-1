package snowflakes

type Point struct {
	x, y int
}

type Triangle struct {
	c      [3]Point
	level  int
	parent *Triangle
}

func RotateTriangle(tr Triangle) Triangle {
	center := Point{(tr.c[0].x + tr.c[1].x + tr.c[2].x) / 3, (tr.c[0].y + tr.c[1].y + tr.c[2].y) / 3}
	var newTriangle Triangle
	newTriangle.c[0].x = tr.c[0].x
	newTriangle.c[0].y = -(tr.c[0].y - center.y) + center.y
	newTriangle.c[1].x = tr.c[1].x
	newTriangle.c[1].y = -(tr.c[1].y - center.y) + center.y
	newTriangle.c[2].x = tr.c[2].x
	newTriangle.c[2].y = -(tr.c[2].y - center.y) + center.y
	return newTriangle
}

func IsPointInTriangle(p Point, tr *Triangle) bool {
	var pl1 = (tr.c[0].x-p.x)*(tr.c[1].y-tr.c[0].y) - (tr.c[1].x-tr.c[0].x)*(tr.c[0].y-p.y)
	var pl2 = (tr.c[1].x-p.x)*(tr.c[2].y-tr.c[1].y) - (tr.c[2].x-tr.c[1].x)*(tr.c[1].y-p.y)
	var pl3 = (tr.c[2].x-p.x)*(tr.c[0].y-tr.c[2].y) - (tr.c[0].x-tr.c[2].x)*(tr.c[2].y-p.y)
	return (pl1 >= 0 && pl2 >= 0 && pl3 >= 0) || (pl1 <= 0 && pl2 <= 0 && pl3 <= 0)
}

func mod(x, y int) int {
	if y < 0 {
		panic("y must be positive")
	}
	z := x / y
	if x < 0 && x%y != 0 {
		z--
	}
	return x - y*z
}

func GenerateTriangles(tr *Triangle, maxLevel, m int, result *int) {
	if tr.level == maxLevel {
		center := Point{(tr.c[0].x + tr.c[1].x + tr.c[2].x) / 3, (tr.c[0].y + tr.c[1].y + tr.c[2].y) / 3}
		thickness := 0
		var t = tr
		for t.parent != nil {
			t = t.parent
			if IsPointInTriangle(center, t) {
				thickness++
			}
			if t.parent == t {
				break
			}
		}
		if thickness == m {
			*result++
		}
		return
	} else {
		rTr := RotateTriangle(*tr)
		rTr.level = tr.level + 1
		rTr.parent = tr.parent

		for i, corner := range tr.c {
			p1 := Point{(tr.c[mod(i-1, 3)].x-corner.x)/3 + corner.x, (tr.c[mod(i-1, 3)].y-corner.y)/3 + corner.y}
			p2 := Point{(tr.c[mod(i+1, 3)].x-corner.x)/3 + corner.x, (tr.c[mod(i+1, 3)].y-corner.y)/3 + corner.y}
			trOuter := Triangle{
				[3]Point{corner, p1, p2},
				rTr.level,
				&rTr,
			}
			GenerateTriangles(&trOuter, maxLevel, m, result)
		}
		for i, corner := range rTr.c {
			p1 := Point{(rTr.c[mod(i-1, 3)].x-corner.x)/3 + corner.x, (rTr.c[mod(i-1, 3)].y-corner.y)/3 + corner.y}
			p2 := Point{(rTr.c[mod(i+1, 3)].x-corner.x)/3 + corner.x, (rTr.c[mod(i+1, 3)].y-corner.y)/3 + corner.y}
			trOuter := Triangle{
				[3]Point{corner, p1, p2},
				rTr.level,
				&rTr,
			}
			GenerateTriangles(&trOuter, maxLevel, m, result)
		}
	}
}

func OverlaidTriangles(n, m int) int {
	tr := Triangle{
		[3]Point{{-43046721, 0}, {0, 57395628}, {43046721, 0}},
		1,
		nil,
	}
	//tr := Triangle{
	//	[3]Point{{-10460353203, 0}, {0, 13947137604}, {10460353203, 0}},
	//	1,
	//	nil,
	//}
	//tr := Triangle{
	//	[3]Point{{-27, 0}, {0, 36}, {27, 0}},
	//	1,
	//	nil,
	//}
	tr.parent = &tr
	var result int
	GenerateTriangles(&tr, n, m, &result)
	return result
}
