package chess

import (
	"errors"
	"fmt"
	"strings"
)

type Vect2 struct {
	x, y int
}

func StrToVect2(str string) (Vect2, error) {
	arr := []rune(str)
	vect2 := Vect2{}
	if len(arr) != 2 {
		return vect2, fmt.Errorf("parse error '%s'", str)
	}
	if arr[0] < 'a' || arr[0] > 'h' {
		return vect2, fmt.Errorf("parse error '%s'", str)
	}
	if arr[1] < '1' || arr[1] > '8' {
		return vect2, fmt.Errorf("parse error '%s'", str)
	}
	vect2.x = int(arr[0] - 'a')
	vect2.y = int(arr[1] - '0')
	return vect2, nil
}

func CanKnightAttack(white, black string) (bool, error) {
	whiteVect, err1 := StrToVect2(strings.ToLower(white))
	if err1 != nil {
		return false, err1
	}
	blackVect, err2 := StrToVect2(strings.ToLower(black))
	if err2 != nil {
		return false, err2
	}
	if whiteVect == blackVect {
		return false, errors.New("horse positions are equal")
	}
	dx := whiteVect.x - blackVect.x
	dy := whiteVect.y - blackVect.y
	if dx*dx+dy*dy == 5 {
		return true, nil
	} else {
		return false, nil
	}
}
