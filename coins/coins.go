package coins

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func GetPilesR(n int, max int, count *int) {
	if n > 0 {
		for i := min(n, max); i >= 1; i-- {
			GetPilesR(n-i, i, count)
		}
	} else {
		*count++
	}
}

func Piles(n int) int {
	var count int
	GetPilesR(n, n, &count)
	return count
}
