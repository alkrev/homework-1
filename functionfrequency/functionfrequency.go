package functionfrequency

import (
	"sort"
	"strings"
)

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func IsLetter(letter rune) bool {
	if (letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z') {
		return true
	} else {
		return false
	}
}

func testFunc(word string) string {
	bracketIndex := strings.Index(word, "(")
	if bracketIndex <= 0 {
		return ""
	}
	if !IsLetter(rune(word[bracketIndex-1])) {
		return ""
	}
	for i := 0; i < bracketIndex; i++ {
		if !IsLetter(rune(word[i])) && word[i] != '.' {
			return ""
		}
	}
	return word[:bracketIndex]
}

type FunctStruct struct {
	name  string
	count int
}

func FunctionFrequency(gocode []byte) []string {
	var result []string
	var sb strings.Builder
	functions := map[string]int{}

	for i := 0; i < len(gocode); i++ {
		if gocode[i] == '\n' || i == len(gocode)-1 {
			line := strings.Trim(sb.String(), " \t\r")
			if line != "" {
				words := strings.Split(line, " ")
				for _, word := range words {
					if word == "func" || word == "//" {
						break
					}
					if funcName := testFunc(word); funcName != "" {
						functions[funcName]++
					}
				}
			}
			sb.Reset()
		} else {
			sb.WriteByte(gocode[i])
		}
	}
	var functionsSorted []FunctStruct
	for key, val := range functions {
		functionsSorted = append(functionsSorted, FunctStruct{key, val})
	}
	sort.Slice(functionsSorted, func(i, j int) bool {
		return functionsSorted[i].count >= functionsSorted[j].count
	})
	for i := 0; i < min(3, len(functionsSorted)); i++ {
		result = append(result, functionsSorted[i].name)
	}
	return result
}
