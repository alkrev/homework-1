package reverseparentheses

type Node struct {
	char  string
	nodes []Node
}

func FillNodes(currentNode *Node, s *string, pos *int, level int) {
	str := *s
	for *pos < len(*s) {
		switch {
		case str[*pos] == '(':
			{
				newNode := Node{}
				*pos++
				FillNodes(&newNode, s, pos, level+1)
				currentNode.nodes = append(currentNode.nodes, newNode)
			}
		case str[*pos] == ')':
			{
				*pos++
				if level%2 == 1 {
					for i, j := 0, len(currentNode.nodes)-1; i < j; i, j = i+1, j-1 {
						currentNode.nodes[i], currentNode.nodes[j] = currentNode.nodes[j], currentNode.nodes[i]
					}
				}
				return
			}
		default:
			{
				newNode := Node{}
				newNode.char = string(str[*pos])
				currentNode.nodes = append(currentNode.nodes, newNode)
				*pos++
			}
		}
	}
}

func GetResult(currentNode *Node, result *string) {
	for _, node := range currentNode.nodes {
		if node.nodes != nil {
			GetResult(&node, result)
		} else {
			*result += node.char
		}
	}
}

func Reverse(s string) string {
	mainNode := Node{}
	var pos = 0
	FillNodes(&mainNode, &s, &pos, 0)
	var result string = ""
	GetResult(&mainNode, &result)

	return result
}
