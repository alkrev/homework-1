package romannumerals

import (
	"sort"
	"strings"
)

type ArabicLatin struct {
	arabic int
	latin  string
}

var arabicLatinTable = []ArabicLatin{
	{1, "I"},
	{2, "II"},
	{3, "III"},
	{4, "IV"},
	{5, "V"},
	{6, "VI"},
	{7, "VII"},
	{8, "VIII"},
	{9, "IX"},
	{10, "X"},
	{20, "XX"},
	{30, "XXX"},
	{40, "XL"},
	{50, "L"},
	{60, "LX"},
	{70, "LXX"},
	{80, "LXXX"},
	{90, "XC"},
	{100, "C"},
	{200, "CC"},
	{300, "CCC"},
	{400, "CD"},
	{500, "D"},
	{600, "DC"},
	{700, "DCC"},
	{800, "DCCC"},
	{900, "CM"},
	{1000, "M"},
	{2000, "MM"},
	{3000, "MMM"},
}

func Encode(n int) (string, bool) {
	if n > 3999 || n < 1 {
		return "", false
	}
	var arabicToLatin map[int]string = map[int]string{}
	var keys []int
	for _, val := range arabicLatinTable {
		keys = append(keys, val.arabic)
		arabicToLatin[val.arabic] = val.latin
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))
	var result string = ""
	for n > 0 {
		for _, key := range keys {
			if key <= n {
				n -= key
				result += arabicToLatin[key]
				break
			}
		}
	}
	return result, true
}

func Decode(s string) (int, bool) {
	var arabicToLatin map[int]string = map[int]string{}
	var keys []int
	for _, val := range arabicLatinTable {
		keys = append(keys, val.arabic)
		arabicToLatin[val.arabic] = val.latin
	}
	sort.Sort(sort.Reverse(sort.IntSlice(keys)))
	var result int = 0
	for s != "" {
		for _, key := range keys {
			if strings.Index(s, arabicToLatin[key]) == 0 {
				result += key
				s = s[len(arabicToLatin[key]):]
				goto label1
			}
		}
		return 0, false
	label1:
	}
	if result == 0 {
		return 0, false
	} else {
		return result, true
	}
}
